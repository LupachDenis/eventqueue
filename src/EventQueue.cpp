#include "EventQueue.h"

class ScopedMutex {
public:
	ScopedMutex(std::mutex& mutex) : _mutex(mutex) {
		mutex.lock();
	}
	~ScopedMutex(){
		if (_locked)
			_mutex.unlock();
	}

	void relock() { 
		_mutex.lock(); 
		_locked = true;
	}
	void unlock() { 
		_mutex.unlock(); 
		_locked = false;
	}
private:
	bool _locked = true;
	std::mutex& _mutex;
	
};

struct TaskData {
	EventQueue* _eventQueue = nullptr;	
	int _interval = 0;
	EventQueue::TaskType _taskType;
	int64_t _nextTime = 0;
	bool _completed = false;
	std::function<void()> _functor;

	TaskData(EventQueue* queue, int interval, EventQueue::TaskType taskType, const std::function<void()>& functor)
		: _eventQueue(queue), _interval(interval), _taskType(taskType), _functor(functor)
	{}
};

Task::Task(const TaskDataPtr& taskData) : _taskData(taskData) {

}

Task::~Task() {
	//cancel();
}

bool Task::completed() const {
	if (EventQueue* queue = _taskData->_eventQueue) {
		std::lock_guard<std::mutex> lock(queue->_mutex);		// mutex
		return _taskData->_completed;
	}
	return true;
}

void Task::cancel() {
	if (EventQueue* queue = _taskData->_eventQueue) {
		queue->cancel(_taskData);
	}
}

EventQueue::EventQueue() {}

void EventQueue::run() {
	while (!_exit) {

		while (!_exit)
		{
			ScopedMutex lock(_mutex);		// mutex
			auto it = _queue.begin();
			if (it == _queue.end())
				break;
			if (it->first > now())
				break;
			TaskDataPtr taskData = it->second;
			_queue.erase(it);
			lock.unlock();		// mutex
			taskData->_functor();
			lock.relock();		// mutex
			if (taskData->_taskType == SingleShot)
				taskData->_completed = true;
			if (!_exit && taskData->_taskType == Periodic && !taskData->_completed) {
				int64_t nextTime = now() + taskData->_interval;
				taskData->_nextTime = nextTime;
				_queue.insert(std::pair<int64_t, TaskDataPtr>(nextTime, taskData));
			}
			lock.unlock();		// mutex
		}

		if (!_exit)
			std::this_thread::sleep_for(std::chrono::milliseconds(1));
	}
}

void EventQueue::quit() {
	_exit = true;
}

Task EventQueue::setInterval(int interval, Callback callback) {
	return addTask(interval, Periodic, callback);
}

Task EventQueue::setTimeout(int timeout, Callback callback) {
	return addTask(timeout, SingleShot, callback);
}

int64_t EventQueue::now() const {
	return static_cast<int64_t>(std::chrono::duration_cast<std::chrono::milliseconds>(
		std::chrono::system_clock::now().time_since_epoch()).count());
}

EventQueue::TaskDataPtr EventQueue::addTask(int interval, EventQueue::TaskType taskType, const EventQueue::Callback & callback) {
	TaskDataPtr taskData(new TaskData(this, interval, taskType, callback));
	int64_t nextTime = now() + taskData->_interval;
	taskData->_nextTime = nextTime;
	_mutex.lock();			// mutex
	_queue.insert(std::pair<int64_t, TaskDataPtr>(nextTime, taskData));
	_mutex.unlock();		// mutex
	return taskData;
}

void EventQueue::cancel(const TaskDataPtr & taskData) {

	std::lock_guard<std::mutex> lock(_mutex);		// mutex

	auto range = _queue.equal_range(taskData->_nextTime);
	for (auto it = range.first; it != range.second; ++it) {
		if (it->second == taskData) {
			_queue.erase(it);
			break;
		}
	}
	if (taskData->_taskType == Periodic)
		taskData->_completed = true;
}
