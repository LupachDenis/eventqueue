#include "EventQueue.h"
#include <iostream>
#include <vector>
#include <math.h>
#ifdef _WIN32
#include <conio.h>
#endif
#include <cassert>
//#include <vld.h>

static int64_t now() {
	return static_cast<int64_t>(std::chrono::duration_cast<std::chrono::milliseconds>(
		std::chrono::system_clock::now().time_since_epoch()).count());
}


static bool testVectors(const std::vector<int>& first, const std::vector<int>& second) {

	if (first.size() != second.size())
		return false;
	for (unsigned i = 0; i < first.size(); ++i) {
		if (labs(first[i] - second[i]) > 10)
			return false;
	}
	return true;
}

static void testSimpleInterval() {

	int64_t startTime = now();
	std::vector<int> vector = { 50, 100, 150, 200 };
	std::vector<int> testVector;

	EventQueue queue;
	Task task1 = queue.setInterval(50, [&testVector, &startTime] { testVector.push_back( int(now() - startTime)); });
	Task taskStop = queue.setTimeout(225, [&queue] {queue.quit(); });
	queue.run();
	assert(!task1.completed());
	assert(taskStop.completed());
	if (testVectors(vector, testVector))
		std::cout << __FUNCTION__ << " passed" << std::endl;
	else
		std::cout << __FUNCTION__ << " failed" << std::endl;
}

static void testTaskOnByOne() {	
	std::vector<int> vector(5);
	for (unsigned i = 0; i < vector.size(); ++i)
		vector[i] = i;
	std::vector<int> testVector;

	EventQueue queue;
	for (int i = (int)vector.size() - 1; i >= 0; --i) {
		queue.setTimeout(i * 50, [&testVector, i] {testVector.push_back(i); });
	}	
	Task taskStop = queue.setTimeout(275, [&queue] {queue.quit(); });
	queue.run();
	
	if (vector == testVector)
		std::cout << __FUNCTION__ << " passed" << std::endl;
	else
		std::cout << __FUNCTION__ << " failed" << std::endl;
}

static void testTaskCancel() {

	int64_t startTime = now();
	std::vector<int> vector = {  };
	std::vector<int> testVector;

	EventQueue queue;
	Task task1 = queue.setInterval(50, [&testVector, &startTime] { testVector.push_back(int(now() - startTime)); });
	Task task2 = queue.setTimeout(50, [&testVector, &startTime] { testVector.push_back(int(now() - startTime)); });
	assert(!task1.completed());
	task1.cancel();
	assert(task1.completed());
	assert(!task2.completed());
	task2.cancel();
	assert(!task2.completed());
	Task taskStop = queue.setTimeout(225, [&queue] {queue.quit(); });
	queue.run();
	if (testVectors(vector, testVector))
		std::cout << __FUNCTION__ << " passed" << std::endl;
	else
		std::cout << __FUNCTION__ << " failed" << std::endl;
}

static void testLongTaskCancel() {

	int64_t startTime = now();
	std::vector<int> vector = {1};
	std::vector<int> testVector;

	EventQueue queue;
	Task task1 = queue.setInterval(50, [&testVector, &startTime] { 
		testVector.push_back(1);
		std::this_thread::sleep_for(std::chrono::milliseconds(500));
	} );
	Task task2 = queue.setTimeout(250, [&task1] { task1.cancel(); });

	Task taskStop = queue.setTimeout(1225, [&queue] {queue.quit(); });
	queue.run();
	if (vector == testVector)
		std::cout << __FUNCTION__ << " passed" << std::endl;
	else
		std::cout << __FUNCTION__ << " failed" << std::endl;
}

static void testAddTaskFromAnotherThread() {
	
	EventQueue queue;
	int counter = 0;

	auto threadBody = [&queue,&counter] {
		for (int i=0;i<10000;++i)
			queue.setTimeout(0, [&counter] {++counter;});
	};
	std::thread t1(threadBody);
	std::thread t2(threadBody);
	std::thread t3([&] {
		t1.join();
		t2.join();
		queue.setTimeout(10, [&queue] {queue.quit(); });
	});
	queue.run();

	t3.join();

	if (counter == 20000)
		std::cout << __FUNCTION__ << " passed" << std::endl;
	else
		std::cout << __FUNCTION__ << " failed" << std::endl;
}

static void testAddAndRemoveTaskFromAnotherThread() {
	EventQueue queue;
	int counter = 0;

	auto threadBody = [&queue, &counter] {
		std::vector<Task> tasks;
		tasks.reserve(10000);
		for (int i = 0; i < 10000; ++i)
			tasks.emplace_back(queue.setTimeout(1000, [&counter] {++counter; }));
		for (int i = 0; i < 10000; ++i)
			tasks[i].cancel();
	};
	std::thread t1(threadBody);
	std::thread t2(threadBody);
	std::thread t3([&] {
		t1.join();
		t2.join();
		queue.setTimeout(10, [&queue] {queue.quit(); });
	});
	queue.setTimeout(0, [] {std::this_thread::sleep_for(std::chrono::milliseconds(1000)); });
	queue.run();

	t3.join();

	if (counter == 0)
		std::cout << __FUNCTION__ << " passed" << std::endl;
	else
		std::cout << __FUNCTION__ << " failed" << std::endl;
}


int main(int argc, char* argv[]) {

	testSimpleInterval();
	testTaskOnByOne();
	testTaskCancel();
	testLongTaskCancel();
	testAddTaskFromAnotherThread();
	testAddAndRemoveTaskFromAnotherThread();
#ifdef _WIN32
	std::cout << "Press any key..." << std::endl;
	_getch();
#endif
	return 0;
}

