#ifndef EVENTQUEUE_H
#define EVENTQUEUE_H

#include <functional>
#include <map>
#include <thread>
#include <mutex>

class EventQueue;
struct TaskData;

class Task
{
	friend class EventQueue;
	using TaskDataPtr = std::shared_ptr<TaskData>;
public:
	Task() = delete;
	~Task();

	bool completed() const;
	void cancel();
private:
	Task(const TaskDataPtr& taskData);
private:
	TaskDataPtr _taskData;
};

class EventQueue
{
	friend class Task;
	friend struct TaskData;
	using TaskDataPtr = std::shared_ptr<TaskData>;
	enum TaskType {
		SingleShot,
		Periodic
	};
public:
	using Callback = std::function<void()>;

	EventQueue();
	EventQueue(const EventQueue&) = delete;
	EventQueue& operator=(const EventQueue&) = delete;

	void run();

	void quit();
	Task setInterval(int interval, Callback callback);
	Task setTimeout(int timeout, Callback callback);
private:
	int64_t now() const;
	TaskDataPtr addTask(int interval, TaskType taskType, const Callback& callback);
	void cancel(const TaskDataPtr& taskData);
private:
	std::mutex _mutex;
	bool _exit = false;
	std::multimap<int64_t, TaskDataPtr> _queue;
	
};

#endif
